// ==UserScript==
// @name         Grand Prix Chart
// @namespace    me.xpaw.grandprixchart
// @version      0.6
// @match        https://store.steampowered.com/grandprix*
// @grant        GM_xmlhttpRequest
// @grant        unsafeWindow
// @require      https://cdnjs.cloudflare.com/ajax/libs/highcharts/7.1.2/highstock.js
// @run-at       document-end
// ==/UserScript==

'use strict';

let chartData = {
	chart: {
		type: 'line',
		backgroundColor: '#202225',
        zoomType: 'x',
		style:
		{
			fontSize: '14px',
			fontWeight: 400,
			fontFamily: 'Motiva Sans',
		}
	},
	time: {
		useUTC: false
	},
	title: {
		text: 'Distance over time',
		style: {
			color: '#D0D3CF'
		}
	},
	xAxis: {
		type: 'datetime',
	},
	yAxis: {
		title: {
			text: 'Distance'
		},
		gridLineWidth: 0,
	},
	plotOptions: {
		series: {
			marker: {
				enabled: false
			}
		}
	},
	tooltip:
	{
		shared: true,
		split: false,
	},
	credits:
	{
		enabled: false,
	},
	series: [{
		name: 'Hare',
		color: '#d0d3cf',
	}, {
		name: 'Tortoise',
		color: '#448e64',
	}, {
		name: 'Corgi',
		color: '#d7a318',
	}, {
		name: 'Cockatiel',
		color: '#447491',
	}, {
		name: 'Pig',
		color: '#cf3a22',
	}]
};

let container = document.createElement( 'div' );
container.className = 'prix_raceboard_ctn';
document.querySelector( '.prix_raceboard_ctn' ).after( container );
const chartDistance = Highcharts.chart( container, chartData );

chartData = JSON.parse( JSON.stringify( chartData ) ); // deep clone lol
chartData.title.text = 'Multiplier over time';
chartData.yAxis.title.text = 'Multiplier';

container = document.createElement( 'div' );
container.className = 'prix_raceboard_ctn';
document.querySelector( '.prix_raceboard_ctn' ).after( container );
const chartMultiplier = Highcharts.chart( container, chartData );

chartData = JSON.parse( JSON.stringify( chartData ) ); // deep clone lol
chartData.title.text = 'Boosts-deboosts over time';
chartData.yAxis.title.text = 'Boosts';
chartData.yAxis.min = undefined;

container = document.createElement( 'div' );
container.className = 'prix_raceboard_ctn';
document.querySelector( '.prix_raceboard_ctn' ).after( container );
const chartBoosts = Highcharts.chart( container, chartData );

const w = unsafeWindow || window;
const OriginalUpdateTeamScoresWithData = w.UpdateTeamScoresWithData;

w.UpdateTeamScoresWithData = function UpdateTeamScoresWithData( data )
{
	OriginalUpdateTeamScoresWithData( data );

	const time = Date.now();

	for ( var i = 0; i < data.scores.length; i++ )
	{
		const oTeamScore = data.scores[ i ];
		const teamId = oTeamScore.teamid - 1;

		chartDistance.series[ teamId ].addPoint( [ time, RoundFloat( oTeamScore.score_dist ) ], false );
		chartMultiplier.series[ teamId ].addPoint( [ time, RoundFloat( oTeamScore.current_multiplier * 1000 ) ], false );
		chartBoosts.series[ teamId ].addPoint( [ time, oTeamScore.current_active_boosts - oTeamScore.current_active_deboosts ], false );
	}

	chartDistance.redraw();
	chartMultiplier.redraw();
	chartBoosts.redraw();
};

GM_xmlhttpRequest( {
    method: 'GET',
    url: 'https://www.sharpprojects.xyz/statsnode/getracedataxpus',
    onload: function( request )
    {
        const dlData = JSON.parse( request.responseText );

        for ( var i = 0; i < 5; i++ )
        {
            const data = dlData.data[ i ];

            for ( var y = 0; y < data.length; y++ )
            {
                const point = data[ y ];

                chartDistance.series[ i ].addPoint( [ point.x * 1000, RoundFloat( point.y ) ], false );
            }
        }

        chartDistance.redraw();
    }
} );

function RoundFloat( x )
{
    return Number( x.toFixed( 2 ) );
}